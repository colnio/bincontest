def qsort(arr):
    if len(arr) <= 1:
        return arr
    else:
        x = arr[int(len(arr) / 2)]
        return qsort([i for i in arr if i < x]) + [i for i in arr if i == x] + qsort([i for i in arr if i > x])

n = int(input())

cards = list(map(int, input().split()))
cards = qsort(cards)

print(sum(cards[n // 2: n :]) - sum(cards[:n // 2:]))