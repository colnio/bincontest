n = list(map(int, list(input())))

n.sort()

k = n.count(0)

if k == 0:
    print(''.join(map(str, n)))

else:
    tmp = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    printed_0 = False
    printed = False
    for i in range(10):
        if i == 0:
            continue
        if n.count(tmp[i]):
            if printed_0 == False and not printed:
                print(tmp[i], end='')
                print('0'*k, end='')
                print(str(tmp[i]) * (n.count(tmp[i]) - 1), end='')
                printed_0 = True
                printed = True
            else:
                print(str(tmp[i]) * n.count(tmp[i]), end='')
            printed = True
            
print()

