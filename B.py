def rec_search(arr, k, l, r):
    if k < arr[l] or k > arr[r]:
        return -1
    elif arr[l] == k:
        return l + 1
    elif arr[r] == k:
        return r + 1
    elif r - l <= 1:
        return 
    else:
        c = (l + r) // 2
        if arr[l] <= k < arr[c]:
            return rec_search(arr, k, l, c)
        
        else:
            return rec_search(arr, k, c, r)


a = list(map(int, input().split()))
k = int(input())

print(rec_search(a, k, l = 0, r = len(a) - 1))