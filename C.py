def find_root(f, a, b, tol):
    eps = 1 / 10**6
    c = (a + b) / 2
    flag = 1
    while f(c) > eps or flag:
        if c - a <= tol:
            flag = 0
        if f(a) * f(c) > 0:
            a = c
        else:
            b = c
        c = (a + b) / 2

    return c


print(find_root(f = lambda x: x**3 - 2*x**2 - x, a = 1, b = 4, tol = 0.0001))